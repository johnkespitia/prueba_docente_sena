<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seguridad extends CI_Controller {

    public function login() {
        $this->load->helper('url');
        $this->load->model('Usuario');
        $this->load->library('session');
        $data = [
            'message' => ''
        ];

        if (!empty($this->session->username)) {
            redirect('seguridad/home/');
        }
        if (!empty($this->input->post('user')) && !empty($this->input->post('pass'))) {
            $user = $this->Usuario->findUserLogin($this->input->post('user'), $this->input->post('pass'));
            if (!empty($user)) {
                unset($user['clave']);
                $this->session->set_userdata($user);
                redirect('seguridad/home/');
            } else {
                $data['message'] = 'Datos inválidos, verifique que la identificación y la clave correspondan a los asignados.';
            }
        }
        $this->load->view('layouts/head');
        $this->load->view('seguridad/login',$data);
        $this->load->view('layouts/foot');
    }

    public function pwrecover() {
        $this->load->helper('url');

        $this->load->view('layouts/head');
        $this->load->view('seguridad/pwrecover');
        $this->load->view('layouts/foot');
    }

    public function home() {
        $this->load->helper('url');
        $this->load->library('session');
        if(empty($this->session->username)){
            redirect('seguridad/login');
        }
        $datah=[
            'msg_head'=>''
        ];
        $this->load->view('layouts/headlogin',$datah);
        $this->load->view('layouts/foot');
    }

    public function logout() {
        $this->load->helper('url');
        $this->load->library('session');
        $this->session->sess_destroy();
        redirect('seguridad/login');
    }

}