<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Programa
 *
 * @author APRENDIZ
 */
class Programa extends CI_Controller{
    public function index(){
        $this->load->helper('url');
        $this->load->model('ProgramaModel');
        $this->load->library('session');
        $programas = $this->ProgramaModel->getProgramaAlcaldia($this->session->alcaldia_id);
        $datah=[
            'msg_head'=>'',
            'progs'=>$programas
        ];
        $this->load->view('layouts/headlogin', $datah);
        $this->load->view('programa/list-prog', $datah);
        $this->load->view('layouts/foot');
    }
}
