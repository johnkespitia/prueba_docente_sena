<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Programa
 *
 * @author APRENDIZ
 */
class ProgramaModel extends CI_Model {

    const TABLA = "programa_social";

    public function __construct() {
        $this->load->database();
    }

    public function getProgramaAlcaldia($alcaldia) {
        $this->db->select('
    `programa_social`.`codigo_actividad`,
    `programa_social`.`nombre_actividad`,
    `programa_social`.`plazas_ofertadas`,
    `programa_social`.`fecha_inicio`,
    `programa_social`.`horas_dedicacion`,
    `programa_social`.`estado`,
    `programa_social`.`responsable`,
    `programa_social`.`registrador`,
    `programa_social`.`tipo_programa_id`
    
');
        $this->db->from('programa_social');
        $this->db->join('usuario', 'programa_social.registrador= usuario.id');
        $this->db->where(' usuario.alcaldia_id', $alcaldia);
        $query = $this->db->get();
//        echo $this->db->last_query();
        return $query->result_array();
    }

}
