<?php

class Usuario extends CI_Model {

    const TABLA = "usuario";

    public function __construct() {
        $this->load->database();
    }

    public function findUserLogin($doc, $pass) {
        $query = $this->db->get_where(self::TABLA, array('username' => $doc, 'password' => md5($pass)));
//        echo $this->db->last_query();
        return $query->row_array();
    }

    public function getUsuario($doc) {
        $query = $this->db->get_where(self::TABLA, array('username' => $doc));
        return $query->row_array();
    }

    public function replaceData($user,$documento) {
        return $this->db->update(self::TABLA, $user," username = '{$documento}' ");
    }

}