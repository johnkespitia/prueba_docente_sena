<br>
<div class="container">
    <div class="jumbotron">
        <h2>Sistema de Programas Sociales de integración</h2>
        <p>Ingrese al sistema y gestione los programas sociales</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button"  data-toggle="modal" data-target="#login-modal" >Ingresar</a></p>
    </div>
</div>  
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1>Ingrese con sus datos</h1><br>
            <form method="post" action="">
                <input type="text" name="user" placeholder="Username">
                <input type="password" name="pass" placeholder="Contraseña">
                <input type="submit" name="login" class="login loginmodal-submit" value="Iniciar Sesión">
            </form>

            <div class="login-help">
                
            </div>
        </div>
    </div>
</div>
<?php
if (!empty($message)) {
    ?>
<script>
$(document).ready(function () {
            $('#login-modal-error').modal('show');
        });
        
</script>
    <div class="modal fade" id="login-modal-error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: block;">
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Error en la información</h1><br>

                <p>
                    <?php echo $message; ?>
                </p>
                <input type="submit" name="login" class="login loginmodal-submit" value="Cerrar" onclick="$('#login-modal-error').modal('hide');">
            </div>
        </div>
    </div>

<?php } ?>