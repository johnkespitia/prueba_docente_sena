<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
         <!-- bootstrap -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>themes/basic/css/style.css">
        <title>Matricula</title>
    </head>
    <body>
        <header class="container">
            <br>
            <div class="container">
                <div class="jumbotron">
                    <h2>Programas Sociales de integración</h2>
                    <h4><?php echo $msg_head ?></h4>
                </div>
            </div> 
            <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                <div class="modal-dialog">
                    <div class="loginmodal-container">
                        <h1>Acerca de Matriculas</h1><br>
                        <div>

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <nav class="container navbar navbar-default">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo base_url(); ?>index.php/seguridad/home">
                </a>
            </div>
            <ul class="nav navbar-nav">
                <li><a href="<?php echo base_url(); ?>index.php/seguridad/home">Inicio </a></li>
                <li><a href="<?php echo base_url(); ?>index.php/programa/index">Programas</a></li>
                <li><a href="<?php echo base_url(); ?>index.php/seguridad/logout" >Salir</a></li>
            </ul>
        </nav>
        <section class="container">